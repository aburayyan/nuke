#!/bin/bash
# Author: Jeffry Johar
# About : nuke is a tool to clear up Openshift resources
# Date  : 13/04/2022

# deployments in an Array
if [ -z "$1" ]; then echo "Usage: nuke <deployment name>"; exit; fi

ArrayDEP=($(oc get deployments | awk '{print $1}' | grep -v workspace | grep -v NAME))

#if value exist in the ArrayDEP
find=$1
if [[ ${ArrayDEP[*]} =~ (^|[[:space:]])"$find"($|[[:space:]]) ]]; then
    echo "Deplyment $1 found"
    echo "Existing Deployment: ${ArrayDEP[@]}"
    echo 'Nuke in progres @#$%^&*()+_!!!!!!!!!!!!!!!!!'
else
    echo "Deplyment $1 does not exist"
    echo "Existing Deployment: ${ArrayDEP[@]}"
    exit
fi

# To be deleted resources
oc delete deployment $1
oc delete service $1
oc delete buildconfig $1
oc delete is $1
oc delete route $1
